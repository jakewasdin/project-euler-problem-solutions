from math import *

#Using Pascal's Triangle for a grid

def total_paths_in_grid(x, y):
    return factorial(x+y)

def duplicate_paths(x,y):
    return factorial(x)*factorial(y)

print total_paths_in_grid(20,20)/duplicate_paths(20,20)
