def sum_of_squares(start, end):
    sum = 0
    for x in xrange(start, end+1):
            sum = sum + (x*x)
    return sum

def square_of_sum(start, end):
    numbers = []
    for x in xrange(start, end+1):
        numbers.append(x)
    return sum(numbers)**2

print square_of_sum(1, 100) - sum_of_squares(1, 100)
