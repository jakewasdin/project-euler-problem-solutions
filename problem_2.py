def compute_fibonacci(max_value):
	fibonacci = []
	fibonacci.append(1)
	fibonacci.append(2)
	while (fibonacci[-1] < max_value):
		new_value = fibonacci[-1] + fibonacci[-2]
		if (new_value < max_value):
			fibonacci.append(new_value)
		else:
			return fibonacci
	return fibonacci

def sum_evens(list):
	sum = 0
	for item in list:
		if (item%2 == 0):
			sum = sum + item
	return sum


print sum_evens(compute_fibonacci(4000000))
