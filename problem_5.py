#number is the number to check if it's divisible by
#start is the first number in range
#end is second number in range
def is_divisible_by(number, start, end):
	for i in xrange(start, end+1):
		if (number%i != 0):
			return False
		elif (i == end):
			return True

def smallest_is_divisible_by(start, end):
	i = 1;
	while (True):
		status = is_divisible_by(i, start, end)
		if (status == False):
			i = i+1
		else:
			return i

print smallest_is_divisible_by(1, 20)
		