def generate_collatz_sequence(number):
	sequence = [number]
	
	i = number
	while (sequence[-1] != 1):
		if (i%2) == 0:
			i = i/2
			sequence.append(i)
		else:
			i = 3*i + 1
			sequence.append(i)

	return sequence

def longest_sequence(up_to):
	largest = 0
	largest_number = 0

	for x in xrange(2, up_to):
		length = len(generate_collatz_sequence(x))
		if length > largest: 
			largest = length
			largest_number = x

	return largest_number
		
print longest_sequence(1000000) 
