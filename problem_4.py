def is_palindrone(number):
    string = str(number)
    if (number%2 == 0):
        if (string[0:len(string)/2][::-1] == string[len(string)/2:len(string)]):
            return True
        else:
            return False
    else:
        if (string[0:(len(string)+1)/2][::-1] == string[len(string)/2:len(string)]):
            return True
        else:
            return False

def palindrone_products_of_three_digit_numbers():
    products = []
    for x in xrange(100, 1000):
        for y in xrange(100, 1000):
            if (is_palindrone(x*y)):
                products.append(x*y)
    return products

print max(palindrone_products_of_three_digit_numbers())
