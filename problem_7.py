def is_prime(number):
	for x in xrange(2, number):
		if (number == 2):
			return True
		if (number%x == 0):
			return False
		elif (x == number-1):
			return True
	return False

def generate_primes(number):
    primes = [2]

    i=0
    while (len(primes) < number):
        if (is_prime(i)):
            primes.append(i)
        i = i+1
    return primes

def get_prime(number):
    return max(generate_primes(number))

print get_prime(10001)
