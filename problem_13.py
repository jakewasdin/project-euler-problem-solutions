def read_numbers_from_file():
	numbers = []
	for line in open("assets/problem_13_numbers.txt"):
		numbers.append(int(line))
	return numbers

def get_first_digits_number(number, digits):
	numbers = list(str(sum(number)))[0:digits]
	return "".join(numbers)

print get_first_digits_number(read_numbers_from_file(), 10)

