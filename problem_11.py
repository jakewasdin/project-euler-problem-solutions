from numpy import *

def read_grid_from_file():
	numbers = []

	for line in open("assets/problem_11_grid.txt"):
		for tab in line.split(" "):		
			numbers.append(int(tab))

	numbers_array = asarray(numbers)
	return numbers_array.reshape((20,20))

def greatest_product(grid):
	products = []
	#check up/down
	for i in xrange(3, 20):
		for j in xrange(0,20):
			product = grid[i, j] * grid[i-1, j] * grid[i-2, j] * grid[i-3, j]
			products.append(product)
	
	#check diagonals
	for i in xrange(0, 17):
		for j in xrange(0, 17):
			product = grid[i, j] * grid[i+1, j+1] * grid[i+2, j+2] * grid[i+3, j+3]
			products.append(product)			

	for i in xrange(3, 20):
		for j in xrange(0, 17):
			product = grid[i, j] * grid[i-1, j+1] * grid[i-2, j+2] * grid[i-3, j+3]
			products.append(product)

	#check left/right
	for i in xrange(0, 20):
		for j in xrange(20, 3):
			product = grid[i, j] * grid[i, j-1] * grid[i, j-2] * grid[i, j-3]
			products.append(product)

	return products

	
	
grid = read_grid_from_file()
print max(greatest_product(grid))


