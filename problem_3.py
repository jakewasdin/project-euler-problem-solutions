def is_prime(number):
	for x in range(2, number):
		if (number == 2):
			return True
		if (number%x == 0):
			return False
		elif (x == number-1):
			return True
	return False

def find_factors(number):
	factors = []
	for x in range(2, number-1):
		if (number%x == 0):
			factors.append(x)
	return factors

def find_prime_factors(number):
	factors = find_factors(number)
	prime_factors = []

	for x in factors:
		if (is_prime(x)):
			prime_factors.append(x)
	
	return prime_factors

print (max(find_prime_factors(600851475143)))



