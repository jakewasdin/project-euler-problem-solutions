# README #

These are some solutions to problems at [Project Euler](https://projecteuler.net/). They are not necessarily the most efficient, but everything in the master branch should result in the correct answer. The majority are written in Python 2, although I may use some other languages from time-to-time.