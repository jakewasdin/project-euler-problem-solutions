def is_prime(number):
	for x in range(2, number):
		if (number == 2):
			return True
		if (number%x == 0):
			return False
		elif (x == number-1):
			return True
	return False

def generate_primes(up_to):
    primes = [2]

    i=0
    while (max(primes) < up_to):
        if (is_prime(i)):
            primes.append(i)
       i = i+1
    return primes[0:-1]

print (sum(generate_primes(2000001)))
