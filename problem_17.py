def get_place (place, number):
	if place == "ones": return number%10
	elif place == "tens": return (number%100)/10
	elif place == "hundreds": return (number%1000)/100
	elif place == "thousands": return (number%10000)/1000
	
def count_letters(word):
	return len(list(word.replace(" ","")))

def number_to_word(number):
	single_digit = { 0: "", 1 : "one", 2 : "two", 3 : "three", 4 : "four", 5 : "five", 6 : "six", 7 : "seven", 8 : "eight", 9 : "nine"}
	teen_digit = { 0 : "ten", 1 : "eleven", 2: "twelve", 3: "thirteen", 4: "fourteen", 5: "fifteen", 6: "sixteen", 7: "seventeen", 8: "eighteen", 9: "nineteen"}
	double_digit = { 0: "", 2 : "twenty", 3 : "thirty", 4 : "forty", 5 : "fifty", 6 : "sixty", 7 : "seventy", 8 : "eighty", 9 : "ninety"}
	
	output = ""
	thousands = get_place("thousands", number)
	hundreds = get_place("hundreds", number)
	tens = get_place("tens", number)
	ones = get_place("ones", number)
	
	if thousands != 0:
		output = output + single_digit[thousands] + " thousand " 
		if (hundreds != 0) or (tens != 0) or (ones != 0):
			output = output + "and "
	if hundreds != 0:
		output = output + single_digit[hundreds] + " hundred "
		if (tens != 0) or (ones != 0):
			output = output + "and "
	if tens == 1:
		output = output + teen_digit[ones]
	if tens != 1:
		output = output + double_digit[tens] + " " + single_digit[ones]
	
	return output
	
def count_word_numbers_between(start, end):
	words = ""
	for i in xrange(start, end+1):
		words = words + number_to_word(i)
		
	return count_letters(words)
	
print count_word_numbers_between(1, 1000)
	
