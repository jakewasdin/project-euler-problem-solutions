def get_digits(number):
	return [int(string) for string in list(str(number))]
	
number = 2**1000
print sum(get_digits(number))
