def get_multiples(multiples_of, up_to):
	multiples = []
	for x in xrange(up_to):
		if (x%multiples_of == 0):
			multiples.append(x)
	return multiples

three_multiples = get_multiples(3, 1000)
five_multiples = get_multiples(5, 1000)
duplicates = list(set(three_multiples).intersection(five_multiples))

print sum(three_multiples) + sum(five_multiples) - sum(duplicates)
