def get_divisors(number):
	divisors = []
	for x in xrange(1, number+1):
		if (number%x) == 0:
			divisors.append(x)
	
	return divisors

def get_triangle_number_sum(number):
	sum = 0
	for x in xrange(number+1):
		sum = sum + x
	
	return sum

def find_divisors_of_over_number(number):
	highest = 0

	i = 0
	while (highest < number):
		highest = len(get_divisors(get_triangle_number_sum(i)))
		if highest > number:
			return i
		i = i+1

print get_triangle_number_sum(find_divisors_of_over_number(500))
