def is_pythagorean_triplet(a, b, c):
    if ((a**2 + b**2) == c**2):
        return True
    else:
        return False

def generate_pythagorean_triplets(up_to):
    triplets = []

    for x in xrange(2, up_to+1):
        for y in xrange(2, up_to+1):
            for z in xrange(2, up_to+1):
                if (is_pythagorean_triplet(x, y, z)):
                    triplets.append((x, y, z))

    return triplets

def find_triplet_equal_to(number):
    triplets = generate_pythagorean_triplets(number/2)
    for triplet in triplets:
        if (sum(triplet) == number):
            return triplet

result = find_triplet_equal_to(1000)
print result[0] * result[1] * result[2]
